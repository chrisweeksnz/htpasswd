FROM alpine:latest
LABEL maintainer="Chris Weeks <chris@weeks.net.nz>"
RUN apk update && \
    apk add apache2-utils
ENTRYPOINT ["/usr/bin/htpasswd"]
